var PROTO_PATH = './helloworld.proto';

var parseArgs = require('minimist');
var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var hello_proto = grpc.loadPackageDefinition(packageDefinition).helloworld;

function main() {
  var argv = parseArgs(process.argv.slice(2), {
    string: 'target'
  });
  var target;
  console.log('argv', argv)
  if (argv.target) {
    target = argv.target;
  } else {
    target = 'localhost:50051';
  }
  console.log('target', target)
  var client = new hello_proto.Greeter(target,
                                       grpc.credentials.createInsecure());
  var user;
  console.log('argv._ ', argv._)
  if (argv._.length > 0) {
    user = argv._[0]; 
  } else {
    user = 'world';
  }

  console.log('user', user)
  client.sayHello({name: user}, function(err, response) {
    console.log('Greeting:', response.message);
  });

  client.getReferenceId({id: '1520wd5dwd58'}, function(err, response) {
    console.log('client getReferenceId :', response.data);
  });
}

main();